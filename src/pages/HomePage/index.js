import React, { useEffect,useRef,useState } from 'react'
import Overlay from 'react-bootstrap/Overlay';
import { Link, useParams } from 'react-router-dom'
import { BackBtn, IconCountry, Word } from '../../assets'
import { Gap } from '../../components'
import "./index.css"
import { useDispatch, useSelector } from 'react-redux'
import { countryFullAction } from '../../services'

const HomePage = () =>
{
    const [show, setShow] = useState(false);
    const [show2, setShow2] = useState(false);
    const [dataTool, setDataTool] = useState()

    const target = useRef(null);
    const dispatch  = useDispatch()
    const countryFullList = useSelector(state => state.countryFullReducer.data.data)
    useEffect(() => {
        dispatch(countryFullAction(params.name))
        
    },[])
    const params = useParams();

    // const ShowToolTip = (id) => {
    //     if(id === 1)
    //     {
    //         setShow(!show)
    //         setDataTool(countryFullList[0]?.altSpellings)
            
    //     }else{
    //         setShow2(!show2)
    //         setDataTool(countryFullList[0]?.borders)
    //     }
    // }

    if(countryFullList)
    {
        return (
            <>
                <div className='container'>
                    
                    <Link to='/Search'>
                        <div className='btn-back '>
                            <div className='d-flex  flex-row  justify-content-center align-items-center'>
                                <img src={BackBtn} className="img-back-btn" />
                                <p className='Text-Home'>Back to HomePage</p>
                            </div>
                        </div>
                    </Link>
    
                    <Gap height={50}/>
                    
                    <div className='d-flex flex-row align-items-center'>
                        <p className='Text-Country'>INDONESIA</p>
                        <Gap width={50} />
                        <img src={IconCountry} className="img-country" />
                    </div>
    
                    <div className='d-flex flex-row'>
                        <div className='IntialCountry'>
                            <p className='Text-Intial-Country'>ID</p>
                        </div>
                        <div className='IntialCountry'>
                            <p className='Text-Intial-Country'>Republic of Indonesia</p>
                        </div>
                        <div className='IntialCountry'>
                            <p className='Text-Intial-Country'>Republik Indonesia</p>
                        </div>
                    </div>
    
                    <Gap height={25} />
    
                    <div className='d-flex flex-row'>
                        <div class="card custome-card ">
                            <div class="card-body">
                                <div className='d-flex flex-row'>
                                    <div>
                                        <p className='Long'>Lat Long</p>
                                        <p className='ValueLong'>{countryFullList[0]?.latlng[0]}, {countryFullList[0]?.latlng[1]}</p>
                                    </div>
                                    <div className='justify-content-center d-flex'>
                                        <img src={Word} className="img-word" />
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        <div class="card custome-card flex-grow-1">
                            <div class="card-body d-flex flex-column p-3">
                                <div className='d-flex flex-row '>
                                    <p className='Text-Key'>Capital:</p>
                                    <Gap width={5} />
                                    <p className='Text-Value'>{countryFullList[0]?.capital[0]}</p>
                                </div>
                                <div className='d-flex flex-row '>
                                    <p className='Text-Key'>Region:</p>
                                    <Gap width={5} />
                                    <p className='Text-Value'>{countryFullList[0]?.region}</p>
                                </div>
                                <div className='d-flex flex-row '>
                                    <p className='Text-Key'>Subregion:</p>
                                    <Gap width={5} />
                                    <p className='Text-Value'>{countryFullList[0]?.subregion}</p>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <Gap height={40} />
    
                    <div className='d-flex flex-row'>
                        <div className='flex-grow-1'>
                            <p className='Long'>Calling Code</p>
                            <p className='ValueLong'>{countryFullList[0]?.area}</p>
                            <p className='desc'><span>{countryFullList[0]?.altSpellings?.length} countries</span> with this calling code</p>
                        </div>
    
                        <div className='flex-grow-1'>
                            <p className='Long'>Currency</p>
                            <p className='ValueLong'>{countryFullList[0]?.currencies.IDR.symbol}</p>
                            <p className='desc'><span>{countryFullList[0]?.borders.length} countries</span> with this currency</p>
                        </div>
                       
                        {/* <Overlay target={target.current} show={show} placement="bottom">
                            {({ placement, arrowProps, show: _show, popper, ...props }) => (
                            <div
                                {...props}
                                style={{
                                position: 'absolute',
                                backgroundColor: '#525252',
                                padding: '2px 10px',
                                color: 'white',
                                borderRadius: 3,
                                ...props.style,
                                }}
                            >
                                {
                                    dataTool.map((item) => (
                                        <p>{item}</p>
                                    ))
                                }
                            </div>
                            )}
                        </Overlay> */}

                        {/* <Overlay target={target.current} show={show2} placement="bottom">
                            {({ placement, arrowProps, show: _show, popper, ...props }) => (
                            <div
                                {...props}
                                style={{
                                position: 'absolute',
                                backgroundColor: '#525252',
                                padding: '2px 10px',
                                color: 'white',
                                borderRadius: 3,
                                ...props.style,
                                }}
                            >
                                {
                                    countryFullList[0]?.altSpellings.map((item) => (
                                        <p>{item}</p>
                                    ))
                                }
                            </div>
                            )}
                        </Overlay> */}
                    </div>
                </div>
            </>
        )
    }else{
        return(
            <div><p>Home</p></div>
        )
    }
    
}

export default HomePage