import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import {SearchIcon} from '../../assets'
import { Gap } from '../../components'
import "./index.css"
import { useDispatch, useSelector } from 'react-redux'
import { countryAction } from '../../services'

const Search = () =>
{
    const [name, setName] = useState("")
    const handleSubmit = (e) => {
        setName(e.target.value)
        var name = e.target.value.toLowerCase();
        dispatch(countryAction(name))
    }

    const dispatch  = useDispatch()
    const countryList = useSelector(state => state.countryReducer.data.data)
    // console.log("items",filterData)
    const filterData = 
        countryList?.filter((post) => {
            if(countryList === '') {
                return post
            } else if(post.name.common){
                return post
            }
        })
    
    return (
        <>
            <div className='wrapper d-flex justify-content-center'>
                <div className='wrapperTextForm'>
                    <p className='text-country'>Country</p>
                        <div className="row gx-5">
                            <div className="col">
                                <div className="wrapper-search p-2 border bg-white">
                                    <input className='input-search' onChange={handleSubmit} placeholder='Search' type="search" name="search" />
                                    <Link to={`/HomePage/${name}`}>
                                        <img src={SearchIcon} className="img-search" />
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <Gap height={12} />
                        {/* {filterData  ?  */}
                            <div class="card">
                                <div class="card-body">
                                {
                                    filterData && filterData.map((post, index) => (
                                        <p>{post.name.common}</p>
                                    ))
                                }
                                </div>
                            </div>
                        {/* // : null} */}
                </div>
            </div>
        </>
    )
}

export default Search