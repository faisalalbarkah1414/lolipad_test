import SearchIcon from './ic_search.png'
import BackBtn from './ic_back.png'
import IconCountry from './ic_country.png'
import Word from './word.png'

export {
    SearchIcon,
    BackBtn,
    IconCountry,
    Word
}