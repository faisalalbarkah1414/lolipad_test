import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { HomePage, Search } from '../pages'

const Router = () =>
{
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={Search} />
                <Route path='/HomePage/:name' exact component={HomePage} />
                <Route path='/Search' exact component={Search} />
            </Switch>
        </BrowserRouter>
    )
}

export default Router