import { combineReducers } from 'redux'
import { loginReducer } from './auth'
import { countryReducer, countryFullReducer } from './country'

const reducers = combineReducers({
    login: loginReducer,
    countryReducer,
    countryFullReducer
})

export default reducers