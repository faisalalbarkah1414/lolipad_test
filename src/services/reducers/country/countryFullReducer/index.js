import { COUNTRY_FULL } from '../../../../utils/actionType'

const initialState = {
    isLoading: false,
    data: false,
    errorMessage: false
}

const countryFullReducer = (state = initialState, action) =>
{
    switch(action.type)
    {
        case COUNTRY_FULL.ATTEMPT:
            return {
                ...state,
                isLoading: action.payload.isLoading
            }
        case COUNTRY_FULL.SUCCEED:
            return {
                ...state,
                data: action.payload.data
            }
        case COUNTRY_FULL.FAILED:
            return {
                ...state,
                errorMessage: action.payload.errorMessage
            }
        default:
            return state
    }
}

export default countryFullReducer