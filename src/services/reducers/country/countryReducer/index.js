import { COUNTRY } from '../../../../utils/actionType'

const initialState = {
    isLoading: false,
    data: false,
    errorMessage: false
}

const countryReducer = (state = initialState, action) =>
{
    switch(action.type)
    {
        case COUNTRY.ATTEMPT:
            return {
                ...state,
                isLoading: action.payload.isLoading
            }
        case COUNTRY.SUCCEED:
            return {
                ...state,
                data: action.payload.data
            }
        case COUNTRY.FAILED:
            return {
                ...state,
                errorMessage: action.payload.errorMessage
            }
        default:
            return state
    }
}

export default countryReducer