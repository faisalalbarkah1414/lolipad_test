import countryReducer from './countryReducer'
import countryFullReducer from './countryFullReducer'

export {
    countryReducer,
    countryFullReducer
}