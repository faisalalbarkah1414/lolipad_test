import axios from 'axios'
import { COUNTRY_FULL, setLoading, setSuccess, setFail } from '../../../../utils'

const countryFullAction = (name) =>
{
    return dispatch =>
    {
        dispatch(setLoading(COUNTRY_FULL.ATTEMPT))

        axios({
            method: 'GET',
            url: `https://restcountries.com/v3.1/name/${name}?fullText=true`,
        })
        .then(result =>
        {
            dispatch(setSuccess(COUNTRY_FULL.SUCCEED, result))
        })
        .catch(error =>
        {
            dispatch(setFail(COUNTRY_FULL.FAILED, error))
        })
    }
}

export default countryFullAction