import axios from 'axios'
import { COUNTRY, setLoading, setSuccess, setFail } from '../../../../utils'

const countryAction = (name) =>
{
    return dispatch =>
    {
        dispatch(setLoading(COUNTRY.ATTEMPT))

        axios({
            method: 'GET',
            url: `https://restcountries.com/v3.1/name/${name}`,
        })
        .then(result =>
        {
            dispatch(setSuccess(COUNTRY.SUCCEED, result))
        })
        .catch(error =>
        {
            dispatch(setFail(COUNTRY.FAILED, error))
        })
    }
}

export default countryAction